import jwt from "jsonwebtoken"
import expressJwt from "express-jwt"

const generateAccessToken = ({config, userInfo}) => {
  return jwt.sign(
    userInfo,
    config.jwtTokenSecret,
    config.jwtTokenTime ? {expiresIn: config.jwtTokenTime} : {}
  )
}

const setToken = (result, req, res, next, config) => {
  const {name, surname, email} = result._data
  const userInfo = {
    name: result._data.name,
    surname: result._data.surname,
    email: result._data.email
  }

  const token = generateAccessToken({config, userInfo})
  next({_data: {...userInfo, ...{token}}})
}

const authenticate = (config) => expressJwt({secret: config.jwtTokenSecret, algorithms: ['sha1', 'RS256', 'HS256']})


export {setToken, authenticate}
