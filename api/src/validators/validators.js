import expressValidator from "express-validator"
import {sendUnauthorizedResponse} from '../utils/responses.js'

const payloadExpressValidator = (req, res, next) => {
  const errors = expressValidator.validationResult(req)

  if(!errors.isEmpty()) {
    const badRequest = errors.array().find(val => undefined === val.value)

    if(badRequest) {
      return res.status(400).json(badRequest)
    }

    return res.status(422).json
  }
  next()
}

const expressJWTValidator = (err, req, res, next, config) => {
  if (err.status === 401) {
    return sendUnauthorizedResponse(res, err)
  }
  next()
}

export {payloadExpressValidator, expressJWTValidator}
