import express from 'express'
import {indexController} from '../controllers/indexController.js'
import {sendOkResponse, sendCreatedResponse, sendDeletedResponse} from '../utils/responses.js'
import expressValidator from 'express-validator'
import {payloadExpressValidator, expressJWTValidator} from '../validators/validators.js'
import {postUserController, postLoginController, putUserController, deleteUserController} from '../controllers/userController.js'
import {setToken, authenticate} from '../validators/auth.js'

export default (config) => {
  const routes = express.Router()

    routes.get('/',
      indexController,
      (result, req, res, next) => sendOkResponse(result, req, res)
    );

    routes.post('/user',
      [
        expressValidator.check('name').isLength({min:4, max:255}),
        expressValidator.check('surname').isLength({min:4, max:255}),
        expressValidator.check('email').isEmail(),
        expressValidator.check('password').isLength({min:6, max:255}),
        expressValidator.check('country').optional().isLength({min:3, max:255}),
        expressValidator.check('phone').optional().isInt(),
        expressValidator.check('postalCode').optional().isInt(),
      ],
      (req, res, next) => payloadExpressValidator(req, res, next, config),
      (req, res, next) => postUserController(req, res, next, config),
      (result, req, res, next) => sendCreatedResponse(result, req, res)
    );

    routes.post('/login',
      [
        expressValidator.check('email').isEmail(),
        expressValidator.check('password').isLength({min:6, max:255}),
      ],
      (req, res, next) => payloadExpressValidator(req, res, next, config),
      (req, res, next) => postLoginController(req, res, next, config),
      (result, req, res, next) => setToken(result, req, res, next, config),
      (result, req, res, next) => sendOkResponse(result, req, res),
    );

    routes.put('/user',
      authenticate(config),
      (err, req, res, next) => expressJWTValidator(err, req, res, next, config),
      [
        expressValidator.check('name').optional().isLength({min:4, max:255}),
        expressValidator.check('surname').optional().isLength({min:4, max:255}),
        expressValidator.check('email').optional().isEmail(),
        expressValidator.check('password').isLength({min:6, max:255}),
        expressValidator.check('newPassword').optional().isLength({min:6, max:255}),
        expressValidator.check('country').optional().isLength({min:3, max:255}),
        expressValidator.check('phone').optional().isInt(),
        expressValidator.check('postalCode').optional().isInt(),
      ],
      (req, res, next) => payloadExpressValidator(req, res, next, config),
      (req, res, next) => putUserController(req, res, next, config),
      (result, req, res, next) => sendCreatedResponse(result, req, res)
    );

    routes.delete('/user',
      authenticate(config),
      (err, req, res, next) => expressJWTValidator(err, req, res, next, config),
      [
        expressValidator.check('email').isEmail(),
      ],
      (req, res, next) => payloadExpressValidator(req, res, next, config),
      (req, res, next) => deleteUserController(req, res, next, config),
      (result, req, res, next) => sendDeletedResponse(result, req, res)
    );

  return routes
}
