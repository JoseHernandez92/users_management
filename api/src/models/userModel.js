import mysql from '../adapters/mysql.js'
import {postUserQuery, getUserQuery, putUserQuery, deleteUserQuery} from '../repositories/userRepository.js'

const postUserModel = ({name, surname, email, password, country, phone, postalCode, conn}) => {
   return mysql
      .execute(postUserQuery({name, surname, email, password, country, phone, postalCode}), conn)
      .then(res => res[1].map(({id, password, ...userInfo}) =>({...userInfo})))
}

const getUserModel = ({email, password, conn}) => {
  return mysql
    .execute(getUserQuery(email),conn)
    .then(res => res.map(({id, ...userInfo}) =>({...userInfo})))
}

const putUserModel = ({name, surname, email, password, country, phone, postalCode, conn}) => {
  return mysql
     .execute(putUserQuery({name, surname, email, password, country, phone, postalCode}), conn)
     .then(res => res[1].map(({id, password, ...userInfo}) =>({...userInfo})))
}

const deleteUserModel = ({email, conn}) => {
  return mysql
     .execute(deleteUserQuery(email), conn)
}

export {postUserModel, getUserModel, putUserModel, deleteUserModel}
