

const sendOkResponse = (result, req, res) => {
  res.status(200).json(result)
 }
const sendCreatedResponse = (result, req, res) => {
   res.status(201).json(result)
  }
const sendUnauthorizedResponse = (res, error) => {
  res.status(401).json(error)
}
const sendDeletedResponse = (result, req, res) => {
  res.status(204).json(result)
}

export {sendOkResponse, sendCreatedResponse, sendUnauthorizedResponse, sendDeletedResponse}
