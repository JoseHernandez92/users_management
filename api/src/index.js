import express from 'express'
import config from './config/index.js'
import routes from './routes/index.js'
import bodyParser from 'body-parser'

const app = express()

process.env.NODE_TLS_REJECT_UNAUTORIZED = '0'

app.use(bodyParser.json({
  limit: process.env.APP_BODY_LIMMIT || config.bodyLimit
}))

app.use(bodyParser.urlencoded({
  extended: false
}))

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  next()
})


app.use('/', routes(config))

let server = {}

  server = app.listen(process.env.PORT || config.port, () => {
    const listeningPort = process.env.PORT || config.port
  console.log(`Listening at http://localhost:${listeningPort}`)
  })

export {app, server}
