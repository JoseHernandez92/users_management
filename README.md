# Users Management

This is an user management API. It is possible to create, modify, and delete users, also, is possible to do login.
For modify and delete users is mandatory to be logged


## Documentation

Running the application for the first time: `docker-compose up --build`

To next times: `docker-compose up`

**Endpoints**
- GET / -> To test the serve
- POST /user -> To create new user
- POST /login -> To login
- PUT /user -> To modify an user
- DELETE /user -> To delete an user

In order to see detailed info:  https://www.getpostman.com/collections/1f54f1894b7ce7564c37

### Development

The docker-compose contains a mysql adminer in comments for development purposes 
